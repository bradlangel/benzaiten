open Core.Std
module QT = Quiz_t
open Re2.Std

type body = QT.body = {
  question: string;
  choices: string array;
  solution: string;
  answer: string;
  difficulty: string;
} with sexp

type quiz = QT.quiz = {
  number: string;
  name: string;
  has_pictures: string;
  questions: body array;
} with sexp

type question = QT.question = {
  question: string;
}

module Quiz_mapping = struct
  type t =
    { main:(string * int) list
    ; alternative:(string * int) list }

  let calc =
    let main =
      [ ("1.1", 1)
      ; ("1.2.1", 2)
      ; ("1.2.2.1", 3)
      ; ("1.2.2.2", 4)
      ; ("1.2.2.3", 5)
      ; ("1.2.3.1", 6)
      ; ("1.2.3.2", 7)
      ; ("1.2.3.3", 8)
      ; ("1.3", 9)
      ; ("2.1", 10)
      ; ("2.2", 11)
      ; ("2.3", 12)
      ; ("2.4", 13)
      ; ("2.5", 14)
      ; ("3.1", 15)
      ; ("3.2", 16)
      ; ("3.3.1", 17)
      ; ("3.3.2", 18)
      ; ("3.3.3", 19)
      ; ("3.3.4", 20)
      ; ("3.3.5", 21)
      ; ("3.3.6", 22)
      ; ("3.3.7", 23)
      ; ("3.3.8", 24)
      ; ("3.3.9", 25)
      ; ("3.4", 26)
      ; ("3.5", 27)
      ; ("3.6", 28)
      ; ("3.7", 29)
      ; ("3.8", 30)
      ; ("3.9", 31)
      ; ("3.10", 32)
      ; ("3.11", 33) ] in
    let alternative =
      [ ("4.1", 34)
      ; ("4.2", 35)
      ; ("4.3", 36)
      ; ("4.4", 37)
      ; ("4.5", 38)
      ; ("4.6", 39)
      ; ("4.7", 40)
      ; ("4.8", 41)
      ; ("4.9", 42)
      ; ("4.10",43)
      ; ("5.1", 44)
      ; ("5.2", 45)
      ; ("5.3", 46)
      ; ("5.4", 47)
      ; ("5.5", 48)
      ; ("6.1", 49)
      ; ("6.2", 50)
      ; ("6.3", 51)
      ; ("6.4", 52)
      ; ("7.1", 53)
      ; ("7.2", 54)
      ; ("7.3", 55)
      ; ("7.4", 56)
      ; ("7.5", 57)
      ; ("7.6", 58)
      ; ("7.7", 59)
      ; ("8.1", 60)
      ; ("8.2", 61)
      ; ("8.3", 62)
      ; ("8.4", 63)
      ; ("8.5", 64)
      ; ("8.6", 65)] in
    { main ; alternative }

  let chem =
    { main = calc.main |> List.mapi ~f:(fun i _ -> Int.to_string (i + 1), (i+1))
    ; alternative = [] }

  let of_subject_string = function
    | "CalcX" -> calc
    | "ChemX" -> chem
    | _ -> failwith "Quiz_mapping:of_string: Subject unrecognized"
end


let answer_map = [("a", 0); ("b", 1); ("c", 2); ("d", 3); ("e", 4); ("f", 5)]
module IOS = struct

  type c = QT.c = { value : bool
                  ; text : string
                  } with sexp

  type q = QT.q = { choices : c array
                  ; sol : string
                  ; body : string
                  ; number : int
                  ; diff : string (* Change to Int *)
                  } with sexp

  type t = QT.t = { name : string
                  ; quiz_no : int
                  ; questions : q array
                  } with sexp

  let parse_answer answer =
    let replace re2 = Re2.replace_exn ~f:(fun x -> "") re2 answer in
    Re2.create_exn ~options:[`Case_sensitive false] "Answer:\\s*"
    |> replace
    |> Re2.replace_exn ~f:(fun x -> "") (Re2.create_exn "\\s*")
    |> List.Assoc.find_exn answer_map

  let parse_quiz_no is_alternative subject quiz_no =
    try
      let mapping = Quiz_mapping.of_subject_string subject in
      if is_alternative
      then List.Assoc.find_exn mapping.Quiz_mapping.alternative quiz_no
      else List.Assoc.find_exn mapping.Quiz_mapping.main quiz_no
    with
    | exn -> print_endline "parse_quiz_no"; raise exn

  let of_answer answer =
    (* Convert from 'Answer: c' to 2 where 0 = a, 1 = b, ... *)
    try
      parse_answer answer
    with
    | exn -> print_endline "of_answer"; raise exn

  let of_choice answer number choice =
    { value = (number = answer)
    ; text = choice }

  let of_body index body =
    try
      let answer = of_answer body.answer in
      let f = of_choice answer in
      { choices = Array.mapi body.choices ~f
      ; sol = body.solution
      ; body = body.question
      ; number = index
      ; diff = body.difficulty }
    with
    | exn -> printf "Question#:%d\n" (index + 1); raise exn

  let of_quiz ?(is_alternative=false) ~subject (quiz : quiz) =
    try
      { name = quiz.name
      ; quiz_no = quiz.number |> parse_quiz_no is_alternative subject
      ; questions = Array.mapi quiz.questions ~f:of_body }
    with
    | exn -> printf "Quiz_no: %s\n" quiz.number; raise exn

  let to_sexp ios_t =
    Sexp.to_string_hum (<:sexp_of< t >> ios_t)

end
