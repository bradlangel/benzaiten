open Core.Std
module PT = Product_identifier_t
module PJ = Product_identifier_j

type app =
  | Physics
  | Calculus
  | Economics

type identifier = PT.identifier =
  { s3_key:string
  ; apple_id:string }

let row_to_identifier row =
  { s3_key = Csv.Row.get row 0
  ; apple_id = Csv.Row.get row 1 }

let create_filename app =
  let name = 
    match app with
    | Physics -> "physics"
    | Calculus -> "calculus"
    | Economics -> "economics" in
  name ^ "_product_identifiers"

let csv_map ~skip_first c ~f=
  c
  |> Csv.of_channel
  |> Csv.Rows.input_all
  |> (function
    | f::r when skip_first -> r
    | l -> l)
  |> List.map ~f

let parse csv app () =
  let ids = In_channel.with_file csv
      ~f:(fun c -> csv_map ~skip_first:true c ~f:row_to_identifier) in
  let json = PT.({ ids }) |> PJ.string_of_product_identifiers in
  let root = "./product_identifiers/" in
  let file_path = Filename.concat root (app ^ ".json") in
  Unix.mkdir_p root;
  Out_channel.write_all file_path ~data:json      

let () =
  Command.basic
    ~summary:" Creates a json file of product identifiers from a csv."
    Command.Spec.(empty
                  +> anon ("<csv>" %:file)
                  +> anon ("<app-type>" %:string)) parse
  |> Command.run ~version:"0.0.1"
