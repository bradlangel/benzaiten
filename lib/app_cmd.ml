open Core.Std
module T = Txt_to_json
module F = Flashcards_to_json

let txt_to_json_command =
  (* Specifies a command that outputs a requisition protobuf file to
     stdout in csv format *)
  Command.basic
    ~summary:"\nConverts a txt document to a json file. Stdout the json file"
    Command.Spec.(empty
                  +> anon ("<subject>" %:string)
                  +> anon ("<txtfile>" %:file)
                  +> flag "-v" (optional file)
                             ~doc:"Video Directory. Default: \
                                   Brad's calculus directory location"
                  +> flag "-a" (optional bool)
                             ~doc:"Alternate quiz mapping. Default is false")
    T.cltool_json

let flash_to_json_command =
  (* Specifies a command that outputs a requisition protobuf file to
     stdout in csv format *)
  Command.basic
    ~summary:"\nConverts a txt document to a json file. Stdout the json file"
    Command.Spec.(empty
		  +> anon ("<txtfile>" %:file))
    F.cltool_json

let () =
  Command.group
    ~summary:"quiz txt file to json output"
    ["txt-to-json", txt_to_json_command; "flash", flash_to_json_command]
  |> Command.run ~version:"0.0.1"
